<%@ page language="java" import="java.util.*" pageEncoding="GB2312"%>
<%@ page import="com.lifedk.dbclass.*" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%
WordMixer wm = new WordMixer();
Map<String,String> questions = wm.getQuestions();
wm.close();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>TestQuestions</title>
    
	<script type="text/javascript" src="JS/question.js"></script>

  </head>
  
  <body>
  <div id="questions">
 	将单词的中文写在单词后的空白中<br/>
  <%
  	Set<String> keys = questions.keySet();
  	for(Iterator<String> i = keys.iterator();i.hasNext();){
  		String key = i.next();
  		String value = questions.get(key);
  %>
  	<%=key %>--<input en="<%=key %>" ch="<%=value %>" type="text" value="" onblur="check(this)"/>&nbsp;<span id="<%=key+"result" %>"></span><br/>
  <% 
  	}
  %>
  </div>
  </body>
</html>
