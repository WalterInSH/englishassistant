<%@ page language="java" import="java.util.*" pageEncoding="GB2312"%>
<%
	request.setCharacterEncoding("GB2312");
	response.setCharacterEncoding("GB2312");
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">

		<title>Edit</title>
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		<script type="text/javascript" src="JS/edit_ajax.js"></script>
	</head>

	<body>
		<h4>增加</h4>
		<div>
			单词:
			<input type="text" id="add_en" />
			中文:
			<input type="text" id="add_ch" />
			难度级别(1~5):
			<input type="text" id="add_level" />
			&nbsp;
			<input type="button" value="添加" onclick="addword()" />
			<span id="add_result"></span>
		</div>
		<hr>
		<h4>删除</h4>
		<div>
			单词:
			<input type="text" id="delete_en" />
			&nbsp;
			<input type="button" value="删除" onclick="deleteword()" />
			<span id="delete_result"></span>
		</div>
		<hr>
		<h4>修改</h4>
		<div>
			单词:
			<input type="text" id="change_en" />
			中文:
			<input type="text" id="change_ch" />
			难度级别(1~5):
			<input type="text" id="change_level" />
			&nbsp;
			<input type="button" value="修改" onclick="changeword()" />
			<span id="change_result"></span>
		</div>
	</body>
</html>
