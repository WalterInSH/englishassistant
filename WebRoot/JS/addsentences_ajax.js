var request;
function addsentences(){
	//新建请求
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				alert("ajax failed");
			}
		}
	}
	
	var st = document.getElementById("sentences").value;
	
	//设置回调函数
	request.onreadystatechange = callback;
	//设置发送方法及url
	request.open("GET","addsentences?st="+st, true);
	//发送
	request.send(null);
}

function callback(){
	if (request.readyState == 4) {
		if (request.status == 200) {
			var resultText = request.responseText;
			var result = document.getElementById("result");
			result.innerHTML = resultText;
		}
	}
}

function search(){
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				alert("ajax failed");
			}
		}
	}
	
	var word = document.getElementById("word").value;
	
	request.onreadystatechange = searchCallback;
	request.open("GET","search?word="+word, true);
	request.send(null);
}

function searchCallback(){
	if (request.readyState == 4) {
		if (request.status == 200) {
			var resultText = request.responseText;
			var result = document.getElementById("SearchResult");
			result.innerHTML = resultText;
		}
	}
}
