var request;
function addword() {
	var en = document.getElementById("add_en").value;
	var ch = document.getElementById("add_ch").value;
	var level = document.getElementById("add_level").value;
	if (en == null || en == "" || ch == null || ch == "" || level == null || level == "")
		return;
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				alert("ajax failed");
			}
		}
	}
	request.onreadystatechange = addcallback;
	request.open("GET","addword?en=" + en + "&ch=" + ch + "&level=" + level, true);
	request.send(null);
}

function addcallback() {
	if (request.readyState == 4) {
		if (request.status == 200) {
			var responseText = request.responseText;
			var result = document.getElementById("add_result");
			result.innerHTML = responseText;
		}
		var en = document.getElementById("add_en");
		var ch = document.getElementById("add_ch");
		var level = document.getElementById("add_level");
		en = ch = level = "";
	}
}

function changeword() {
	var en = document.getElementById("change_en").value;
	var ch = document.getElementById("change_ch").value;
	var level = document.getElementById("change_level").value;
	if (en == null || en == "" || ch == null || ch == "" || level == null || level == "")
		return;
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				alert("ajax failed");
			}
		}
	}
	request.onreadystatechange = changecallback;
	//以防读取缓存页面，所以每次发送时多带一个时间参数  
	request.open("GET","changeword?en=" + en + "&ch=" + ch + "&level=" + level, true);
	request.send(null);
}

function changecallback() {
	if (request.readyState == 4) {
		if (request.status == 200) {
			var responseText = request.responseText;
			var result = document.getElementById("change_result");
			result.innerHTML = responseText;
		}
		var en = document.getElementById("change_en");
		var ch = document.getElementById("change_ch");
		var level = document.getElementById("change_level");
		en.value = ch.value = level.value = "";
	}
}

function deleteword() {
	var en = document.getElementById("delete_en").value;
	if (en == null || en == "")
		return;
	try {
		request = new XMLHttpRequest();
	} catch (tryMS) {
		try {
			request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (otherMS) {
			try {
				request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (failed) {
				alert("ajax failed");
			}
		}
	}
	request.onreadystatechange = deletecallback;
	//以防读取缓存页面，所以每次发送时多带一个时间参数  
	request.open("GET","deleteword?en=" + en, true);
	request.send(null);
}

function deletecallback() {
	if (request.readyState == 4) {
		if (request.status == 200) {
			var responseText = request.responseText;
			var result = document.getElementById("delete_result");
			result.innerHTML = responseText;
		}
		var en = document.getElementById("delete_en");
		en.value = "";
	}
}