import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class WordAdder extends HttpServlet {
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		request.setCharacterEncoding("GB2312");
		response.setCharacterEncoding("GB2312");
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		String en = request.getParameter("en").trim();
		String ch = new String(request.getParameter("ch").getBytes("ISO-8859-1"), "GB2312"); 
		String level = request.getParameter("level").trim();
		
		HttpSession session = request.getSession();
		
		String username = (String) session.getAttribute("username");
		
		String result = "";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EnglishAssistant","root","haohaoguo");
			pstmt = conn.prepareStatement("select * from words where word = ?");
			pstmt.setString(1, en);
			rs = pstmt.executeQuery();
			if(rs.next() == true){
				result = "单词已存在";
			}else{
				if(username == null || username ==""){
					pstmt = conn.prepareStatement("insert into words values(null,?,?,?,null)");
					pstmt.setString(1, en);
					pstmt.setString(2, ch);
					pstmt.setInt(3, Integer.parseInt(level));
				}else{
					pstmt = conn.prepareStatement("insert into words values(null,?,?,?,?)");
					pstmt.setString(1, en);
					pstmt.setString(2, ch);
					pstmt.setInt(3, Integer.parseInt(level));
					pstmt.setInt(4, (Integer)(session.getAttribute("userid")));
				}
				pstmt.execute();
				result = "添加成功";
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				if(rs != null){
					rs.close();
					rs = null;
				}
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		out.println(result);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
