package com.lifedk.dbclass;

import java.sql.*;
import java.util.*;


public class WordMixer {
	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	
	public WordMixer(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EnglishAssistant","root","haohaoguo");
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select * from words order by rand() limit 10");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String,String> getQuestions(){
		Map<String,String> question = new HashMap<String, String>();
		try {
			while(rs.next()){
				question.put(rs.getString("word"), rs.getString("chinese"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return question;
	}
	
	public void close(){
		try {
			if(rs != null){
				rs.close();
				rs = null;
			}
			if(stmt != null){
				stmt.close();
				stmt = null;
			}
			if(conn != null){
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
