import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;


public class AddSentences extends HttpServlet {
	
	/*
	 * Servlet通过JDBC对数据库进行处理
	 * Connection类是servlet和数据库的链接
	 * PreparedStatement类定义要在MySQL中执行的SQL语句
	 * ResultSet用于存放执行ＳＱＬ语句得到的结果集
	 */	
	private Connection conn = null;
	private PreparedStatement pstmt = null;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//解决乱码
		request.setCharacterEncoding("GB2312");
		response.setCharacterEncoding("GB2312");
		response.setContentType("text/html");
		
		String sentences = request.getParameter("st");
		
		PrintWriter out = response.getWriter();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//建立链接
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EnglishAssistant","root","haohaoguo");
			//创建并设置SQL语句
			pstmt = conn.prepareStatement("INSERT INTO sentences VALUES(null,?)");
			pstmt.setString(1, sentences);
			//执行
			pstmt.execute();
			//返回执行结果
			out.println("增加成功");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if(conn != null){
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
