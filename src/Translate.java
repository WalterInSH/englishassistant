import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.*;


public class Translate extends HttpServlet {
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("gbk");
		PrintWriter out = response.getWriter();
		String en = request.getParameter("en");
		en = en.trim();
		String[] words = en.split(" ");
		String ch = "";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EnglishAssistant","root","haohaoguo");
			pstmt = conn.prepareStatement("select * from words where word = ?");
			for (int i = 0; i < words.length; i++) {
				pstmt.setString(1, words[i]);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					String str = rs.getString("chinese");
					if (str == null || str == "") {
						ch += "?";
					} else {
						ch += str;
					}
				}
			}
			out.println(ch);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			try {
				if(rs != null){
					rs.close();
					rs = null;
				}
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
