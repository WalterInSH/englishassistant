import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login extends HttpServlet {
	
	private Connection conn = null;
	private ResultSet rs = null;
	private PreparedStatement pstmt = null;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		request.setCharacterEncoding("gbk");
		PrintWriter out = response.getWriter();
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EnglishAssistant","root","haohaoguo");
			pstmt = conn.prepareStatement("SELECT * FROM users WHERE name = ?");
			pstmt.setString(1, username);
			rs = pstmt.executeQuery();
			while(rs.next()){
				if(rs.getString("password") == null){
					out.println("Incorrect user name");
				}else{
					if(rs.getString("password").equals(password)){
						HttpSession session = request.getSession();
						session.setAttribute("isLogin", "true");
						session.setAttribute("username", username);
						session.setAttribute("userid", rs.getInt("id"));
						out.println("success_"+username);
					}else{
						out.println("Incorrect password");
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null){
					rs.close();
					rs = null;
				}
				if(pstmt != null){
					pstmt.close();
					pstmt = null;
				}
				if (conn != null) {
					conn.close();
					conn = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
